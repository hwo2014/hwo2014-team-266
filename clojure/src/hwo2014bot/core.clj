(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(def pingback 
  {:msgType "ping" :data "ping"})

(defmulti handle-msg :msgType)

(defmethod handle-msg "carPositions" [msg]
  {:msgType "throttle" :data 0.625})

(defmethod handle-msg "gameInit" [msg]
  (println msg)
  pingback)

(defmethod handle-msg "carPositions" [msg]
  (let [
    car (first (:data msg))
    position (:piecePosition car)]
  (spit (str "../data/record.csv")
    (str (clojure.string/join ";" [
        (:startLaneIndex (:lane position))
        (:gameTick msg)
        (:lap position)
        (:pieceIndex position)
        (:inPieceDistance position)
        (:angle car)])
      "\n")
    :append true))
  pingback)

(defmethod handle-msg :default [msg]
  pingback)
 
(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameInit" (println "Game initialized")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (println "Race ended")
    "tournamentEnd" (println "Tournament Ended")
    "lapFinished" (println "Finished a lap") 
    "error" (println (str "ERROR: " (:data msg)))
    "yourCar" nil
    "carPositions" nil
    (println "Unhandled message: " msg)))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (send-message channel (handle-msg msg))
    (recur channel)))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel)))
